#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

void Trackbar(int,void*);
Mat src,dst;

const int dMaxvalue=20;
const int sigmaColorMaxvalue=200;
const int sigmaSpaceMaxvalue=50;

int dTrackbarvalue=0;
int colorTrackbarvalue=0;
int spaceTrackbarvalue=0;

int main(int argc, const char *argv[])
{
	src=imread("/home/chh001/img/1.jpg");
	if(!src.data)
	{
		printf("load img error\n");
		return -1;
	}
	imshow("input",src);

	Trackbar(dTrackbarvalue,0);
	createTrackbar("d","双边滤波",&dTrackbarvalue,
			dMaxvalue,Trackbar);
	Trackbar(colorTrackbarvalue,0);
	createTrackbar("sigmaColor","双边滤波",&colorTrackbarvalue,
			sigmaColorMaxvalue,Trackbar);
	Trackbar(dTrackbarvalue,0);
	createTrackbar("sigmaSpace","双边滤波",&spaceTrackbarvalue,
			sigmaSpaceMaxvalue,Trackbar);

	waitKey(0);
	return 0;
}

void Trackbar(int,void*)
{
	int d=0;
	double color=0.0;
	double space=0.0;

	d=dTrackbarvalue;
	color=colorTrackbarvalue;
	space=spaceTrackbarvalue;

	bilateralFilter(src,dst,d,color,space);

	imshow("双边滤波",dst);

}
