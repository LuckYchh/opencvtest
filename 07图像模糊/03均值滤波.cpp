#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

void blurTrackbar(int,void*);
Mat src,dst;

const int widthTrackbarMaxvalue=15;
const int heightTrackbarMaxvalue=15;

int widthTrackbarvalue=0;
int heightTrackbarvalue=0;

int main(int argc, const char *argv[])
{
	src=imread("/home/imx/pic/1.jpg");
	if(!src.data)
	{
		printf("load img error\n");
		return -1;
	}
	imshow("input",src);

	blurTrackbar(widthTrackbarvalue,0);
	createTrackbar("kernelWidth","均值滤波",&widthTrackbarvalue,
			widthTrackbarMaxvalue,blurTrackbar);

	blurTrackbar(heightTrackbarvalue,0);
	createTrackbar("kernelHeigh","均值滤波",&heightTrackbarvalue,
			heightTrackbarMaxvalue,blurTrackbar);
	waitKey(0);
	return 0;
}

void blurTrackbar(int,void*)
{
	int kernelWidthvalue=0;
	int kernelHeightvalue=0;

	kernelWidthvalue=widthTrackbarvalue+1;
	kernelHeightvalue=heightTrackbarvalue+1;

	blur(src,dst,Size(kernelWidthvalue,kernelHeightvalue),Point(-1,-1),BORDER_REPLICATE);
	imshow("均值滤波",dst);

}
