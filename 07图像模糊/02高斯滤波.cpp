#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


void GaussianBlurTrackbar(int, void*);       //定义回调函数
Mat src,dst;

const int gwidthTrackBarMaxValue = 15;
const int gheightTrackBarMaxValue = 15;
const int gsigmaXTrackBarMaxValue = 15;
const int gsigmaYTrackBarMaxvalue = 15;

//定义每个轨迹条的初始值
int gwidthTrackBarValue = 0;
int gheightTrackBarValue = 0;
int gsigmaXTrackBarValue = 0;
int gsigmaYTrackBarValue = 0;


int main()
{
    src = imread("/home/chh001/img/5.jpg");
    if(!src.data)
    {
        printf("load img error\n");
        return -1;
    }
    imshow("input", src);

	//创建轨迹条
    GaussianBlurTrackbar(gwidthTrackBarValue, 0);
    createTrackbar("kernelWidth", "高斯滤波图像", &gwidthTrackBarValue, 
                    gwidthTrackBarMaxValue,GaussianBlurTrackbar);

    GaussianBlurTrackbar(gheightTrackBarValue, 0);
    createTrackbar("kernelHeight", "高斯滤波图像", &gheightTrackBarValue,
                    gheightTrackBarMaxValue,GaussianBlurTrackbar);

    GaussianBlurTrackbar(gsigmaXTrackBarValue, 0);
    createTrackbar("sigmaX", "高斯滤波图像", &gsigmaXTrackBarValue,
                    gsigmaXTrackBarMaxValue,GaussianBlurTrackbar);

    GaussianBlurTrackbar(gsigmaYTrackBarValue, 0);
    createTrackbar("sigmaY", "高斯滤波图像", &gsigmaYTrackBarValue,
                    gsigmaYTrackBarMaxvalue,GaussianBlurTrackbar);

    waitKey(0);

    return 0;
}

void GaussianBlurTrackbar(int, void*)
{
	int gkernelWidthValue=0;
	int gkernelHeightValue=0;
	//根据输入的width和height重新计算ksize.width和ksize.height,记住要为正奇数。
    gkernelWidthValue = gwidthTrackBarValue*2+1;
    gkernelHeightValue = gheightTrackBarValue*2+1;

    //高斯滤波
    GaussianBlur(src, dst, Size(gkernelWidthValue, gkernelHeightValue),
                gsigmaXTrackBarValue, gsigmaYTrackBarValue);

    imshow("高斯滤波图像", dst);
}
