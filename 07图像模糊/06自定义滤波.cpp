#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat img=imread("/home/chh001/img/1.jpg");
	Mat Rdst,Sxdst,Sydst,Ldst;
	imshow("input",img);
	
	Mat Sxkernel=(Mat_<char>(3,3)<<-1,0,1,-2,0,2,-1,0,1);
	filter2D(img,Sxdst,img.depth(),Sxkernel);
	imshow("SobelX",Sxdst);
	Mat Sykernel=(Mat_<char>(3,3)<<-1,-2,-1,0,0,0,1,2,1);
	filter2D(img,Sydst,img.depth(),Sykernel);
	imshow("SobelY",Sydst);
	Mat Lkernel=(Mat_<char>(3,3)<<0,-1,0,-1,4,-1,0,-1,0);
	filter2D(img,Ldst,img.depth(),Lkernel);
	imshow("lapulasi",Ldst);
	waitKey(0);
	return 0;
}
