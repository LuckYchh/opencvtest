#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
	if(!src.data)
	{
		printf("load img error\n");
		return -1;
	}
	imshow("input",src);

	Mat blurdst=Mat::zeros(src.size(),src.type());
	blur(src,blurdst,Size(10,10),Point(-1,-1));
	imshow("blur",blurdst);

	Mat gblurdst=Mat::zeros(src.size(),src.type());
	GaussianBlur(src,gblurdst,Size(11,11),11,500);
	imshow("gblur",gblurdst);
	waitKey(0);
	return 0;
}
