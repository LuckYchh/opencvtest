#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

void medianblurTrackbar(int,void*);
Mat src,dst;

const int ksizeMaxvalue=3;

int ksizeTrackbarvalue=0;

int main(int argc, const char *argv[])
{
	src=imread("/home/chh001/img/5.jpg");
	if(!src.data)
	{
		printf("load img error\n");
		return -1;
	}
	imshow("input",src);

	medianblurTrackbar(ksizeTrackbarvalue,0);
	createTrackbar("ksize","中值滤波",&ksizeTrackbarvalue,
			ksizeMaxvalue,medianblurTrackbar);

	waitKey(0);
	return 0;
}

void medianblurTrackbar(int,void*)
{
	int ksizeValue=0;

	ksizeValue=ksizeTrackbarvalue*2+1;

	medianBlur(src,dst,ksizeValue);
	imshow("中值滤波",dst);

}
