#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	int typevalue=0;
	int method=0;
	Mat src=imread("/home/imx/pic/1.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
	}
	imshow("input",src);
	Mat dst;
	cvtColor(src,src,COLOR_BGR2GRAY);
	printf("========Threshold Type==========\n");
	printf("1 THRESH_BINARY\n");
	printf("2 THRESH_BINARY_INV\n");
	printf("========Adaptive Method==========\n");
	printf("3 ADAPTIVE_THRESH_MEAN_C\n");
	printf("4 ADAPTIVE_THRESH_GAUSSIAN_C \n");
	char c;
	while(true)
	{
		c=waitKey(500);
		if(c==27)
			break;
		if(c==49)
			typevalue=THRESH_BINARY;
		else if(c==50)
			typevalue=THRESH_BINARY_INV;
		else if(c==51)
			method=ADAPTIVE_THRESH_MEAN_C;
		else if(c==52)
			method=ADAPTIVE_THRESH_GAUSSIAN_C;
		adaptiveThreshold(src,dst,255,method,typevalue,7,0);
		imshow("adaptiveThreold",dst);
		waitKey(0);
	}

	return 0;
}

