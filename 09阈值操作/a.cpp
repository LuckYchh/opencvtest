#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

int typevalue=0;

int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
	}
	imshow("input",src);
	Mat dst;
	cvtColor(src,src,CV_BGR2GRAY);
	printf("========MENU==========\n");
	printf("1 THRESH_BINARY\n");
	printf("2 THRESH_BINARY_INV\n");
	printf("3 THRESH_TRUNC\n");
	printf("4 THRESH_TOZERO\n");
	printf("5 THRESH_TOZERO_INV\n");
	char c;
	while(true)
	{
		c=waitKey(500);
		if(c==27)
			break;
		if(c==49)
			typevalue=THRESH_BINARY;
		else if(c==50)
			typevalue=THRESH_BINARY_INV;
		else if(c==51)
			typevalue=THRESH_TRUNC;
		else if(c==52)
			typevalue=THRESH_TOZERO;
		else if(c==53)
			typevalue=THRESH_TOZERO_INV;
		threshold(src,dst,180,255,typevalue);
		imshow("阈值处理",dst);
	}

	waitKey(0);
	return 0;
}

