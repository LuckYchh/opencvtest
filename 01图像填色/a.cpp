#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/3.jpg");
	if(src.empty())
	{
		printf("can not find the image\n");
		return -1;
	}

	printf("read image success\n");
	int height=src.rows;
	int width=src.cols;
	int channels=src.channels();
	printf("h:%d w:%d ch:%d\n",height,width,channels);
	for(int row=0;row<height;row++)
	{
		for(int col=0;col<width;col++)
		{
			if(channels==3)
			{
				src.at<Vec3b>(row,col)[0]=200;
				src.at<Vec3b>(row,col)[1]=80;
				src.at<Vec3b>(row,col)[2]=180;
			}
		}
	}
	imwrite("a.png",src);
	namedWindow("SRC",CV_WINDOW_AUTOSIZE);
	imshow("SRC",src);

	waitKey(0);
	return 0;
}
