#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/14.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
		return -1;
	}
	imshow("input",src);

	Mat dst;
	medianBlur(src,dst,3);
	cvtColor(dst,dst,CV_BGR2GRAY);
	vector<Vec3f> hcircle;

	HoughCircles(dst,hcircle,CV_HOUGH_GRADIENT,1,10,100,28,1,50);

	cvtColor(dst,dst,CV_GRAY2BGR);
	for(int i=0;i<hcircle.size();i++)
	{
		Vec3f cc=hcircle[i];
		//圆心
		circle(dst,Point(cc[0],cc[1]),2,Scalar(123,67,89),2,LINE_AA);
		//半径
		circle(dst,Point(cc[0],cc[1]),cc[2],Scalar(0,0,255),2,LINE_AA);
	}

	imshow("HoughCircles",dst);
	waitKey(0);
	return 0;
}
