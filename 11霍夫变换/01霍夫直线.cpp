#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main()
{
	Mat src,dst,gray; 
	src=imread("/home/imx/pic/line2.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
		return -1;
	}
	imshow("input",src);

	printf("----------------\n");
	printf("1 HoughLines\n");
	printf("2 HoughLinesP\n");

	char c = 0;
	while(true)
	{
		c = waitKey(500);
		if(c == 49)
		{
			cvtColor(src,gray,COLOR_BGR2GRAY);
			Canny(gray,dst,100,200,3);
			vector<Vec2f> lines;
			HoughLines(dst,lines,1,CV_PI/180,90);
			cvtColor(dst,dst,COLOR_GRAY2BGR);
			for(int i=0;i<lines.size();i++)
			{
				float r=lines[i][0];
				float theta=lines[i][1];
				Point pt1,pt2;
				double a=cos(theta),b=sin(theta);
				double x0=a*r,y0=b*r;
				pt1.x=cvRound(x0+1000*(-b));
				pt1.y=cvRound(y0+1000*a);
				pt2.x=cvRound(x0-1000*(-b));
				pt2.y=cvRound(y0-1000*a);
				line(dst,pt1,pt2,Scalar(0,255,0),1,LINE_AA);
			}
			imshow("HoughLines test",dst);
		}
		if(c == 50)
		{
			cvtColor(src,gray,COLOR_BGR2GRAY);
			Canny(gray,dst,100,200,3);
			vector<Vec4f> plines;
			HoughLinesP(dst,plines,1,CV_PI/180,50,50,10);
			cvtColor(dst,dst,COLOR_GRAY2BGR);
			for(int i=0;i<plines.size();i++)
			{
				Vec4i l = plines[i];
				line(dst,Point(l[0],l[1]),Point(l[2],l[3]),Scalar(0,255,0),1,LINE_AA);
			}
			imshow("HoughLinesP test",dst);
		}
	}
	waitKey(0);
	return 0;
}






































