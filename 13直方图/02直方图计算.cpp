#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
	}
	imshow("input",src);

	vector<Mat> pic;
	split(src,pic);
//	imshow("a",pic[0]);
//	imshow("b",pic[1]);
//	imshow("c",pic[2]);

	int histvalue=255;
	float range[]={0,255};
	const float *histrange={range};
	Mat b,g,r;
	calcHist(&pic[0],1,0,Mat(),b,1,&histvalue,&histrange,true,false);
	calcHist(&pic[1],1,0,Mat(),g,1,&histvalue,&histrange,true,false);
	calcHist(&pic[2],1,0,Mat(),r,1,&histvalue,&histrange,true,false);

	int hist_h=240;
	int hist_w=240;
	int bin=hist_w/histvalue;
	Mat histimg(hist_w,hist_h,CV_8UC3,Scalar(0,0,0));
	normalize(b,b,0,hist_h,NORM_MINMAX,-1,Mat());
	normalize(g,g,0,hist_h,NORM_MINMAX,-1,Mat());
	normalize(r,r,0,hist_h,NORM_MINMAX,-1,Mat());

	for(int i=1;i<histvalue;i++)
	{
		line(histimg,Point((i-1)*bin,hist_h-cvRound(b.at<float>(i-1))),
					Point((i)*bin,hist_h-cvRound(b.at<float>(i))),
				Scalar(255,0,0),2,LINE_AA);
		line(histimg,Point((i-1)*bin,hist_h-cvRound(g.at<float>(i-1))),
					Point((i)*bin,hist_h-cvRound(g.at<float>(i))),
				Scalar(0,255,0),2,LINE_AA);
		line(histimg,Point((i-1)*bin,hist_h-cvRound(r.at<float>(i-1))),
					Point((i)*bin,hist_h-cvRound(r.at<float>(i))),
				Scalar(0,0,255),2,LINE_AA);
	}
	imshow("histimg",histimg);
	waitKey(0);
	return 0;
}
