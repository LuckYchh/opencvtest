#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
//	cvtColor(src,src,CV_BGR2GRAY);
	imshow("input",src);

	Mat dst;
	dst=Mat::zeros(src.size(),src.type());
	int height=src.rows;
	int width=src.cols;
	float alpha=1.2;
	float beta=10;

	Mat m1;
	src.convertTo(m1,CV_32F);
	for(int row=0;row<height;row++)
		for(int col=0;col<width;col++)
		{
			if(src.channels()==3)
			{
				
				float b=m1.at<Vec3f>(row,col)[0];
				float g=m1.at<Vec3f>(row,col)[1];
				float r=m1.at<Vec3f>(row,col)[2];

				dst.at<Vec3b>(row,col)[0]=saturate_cast<uchar>(b*alpha+beta);
				dst.at<Vec3b>(row,col)[1]=saturate_cast<uchar>(g*alpha+beta);
				dst.at<Vec3b>(row,col)[2]=saturate_cast<uchar>(r*alpha+beta);
				
			}
			else if(src.channels()==1)
			{
				float v=src.at<uchar>(row,col);
				dst.at<uchar>(row,col)=saturate_cast<uchar>(v*alpha+beta);

			}
		}

	imshow("output",dst);
	waitKey(0);
	return 0;
}
