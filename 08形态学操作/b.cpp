#include <opencv2/opencv.hpp>
#include <iostream>
#include "math.h"

using namespace cv;

void TrackbarUp(int,void*);
void TrackbarDown(int,void*);
Mat src,dst;
const int wmax=3 ;
const int hmax=3;
int wTrackbar=1;
int hTrackbar=1;

int main(int agrc, char** argv) {
	src = imread("/home/chh001/img/1.jpg");
	if (!src.data) {
		printf("could not load image...");
		return -1;
	}
	imshow("src", src);
	
	pyrUp(src, dst, Size(src.cols*2, src.rows*2));
	imshow("up", dst);
	pyrDown(src,dst, Size(src.cols/2, src.rows/2));
	imshow("dst", dst);

	waitKey(0);
	return 0;
}
