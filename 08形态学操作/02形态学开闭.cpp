#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

void TrackbarOpen(int,void*);
void TrackbarClose(int,void*);
void TrackbarGradient(int,void*);
void TrackbarTophat(int,void*);
void TrackbarBlackhat(int,void*);
Mat src,dst;

const int sizeMaxvalue=50;
int sizeTrackbar=1;

int main(int argc, const char *argv[])
{
	src=imread("/home/chh001/img/19.jpg");
	imshow("input",src);

	TrackbarGradient(sizeTrackbar,0);
	createTrackbar("梯度","形态学开闭",&sizeTrackbar,
			sizeMaxvalue,TrackbarGradient);
/*	TrackbarTophat(sizeTrackbar,0);
	createTrackbar("顶帽","形态学开闭",&sizeTrackbar,
			sizeMaxvalue,TrackbarTophat);
	TrackbarBlackhat(sizeTrackbar,0);
	createTrackbar("黑帽","形态学开闭",&sizeTrackbar,
			sizeMaxvalue,TrackbarBlackhat);
	TrackbarOpen(sizeTrackbar,0);
	createTrackbar("开","形态学开闭",&sizeTrackbar,
			sizeMaxvalue,TrackbarOpen);
	TrackbarClose(sizeTrackbar,0);
	createTrackbar("闭","形态学开闭",&sizeTrackbar,
			sizeMaxvalue,TrackbarClose);*/
	waitKey(0);
	return 0;
}
void TrackbarOpen(int,void*)
{
	int size=sizeTrackbar;
	if(size<1)
		size=1;
	Mat kernel=getStructuringElement(MORPH_RECT,Size(size,size));
	morphologyEx(src,dst,MORPH_OPEN,kernel);
	imshow("形态学开闭",dst);
}

void TrackbarClose(int,void*)
{
	int size=sizeTrackbar;
	if(size<1)
		size=1;
	Mat kernel=getStructuringElement(MORPH_RECT,Size(size,size));
	morphologyEx(src,dst,MORPH_CLOSE,kernel);
	imshow("形态学开闭",dst);
}

void TrackbarGradient(int,void*)
{
	int size=sizeTrackbar;
	if(size<1)
		size=1;
	Mat kernel=getStructuringElement(MORPH_RECT,Size(size,size));
	morphologyEx(src,dst,MORPH_GRADIENT,kernel);
	imshow("形态学开闭",dst);
}

void TrackbarTophat(int,void*)
{
	int size=sizeTrackbar;
	if(size<1)
		size=1;
	Mat kernel=getStructuringElement(MORPH_RECT,Size(size,size));
	morphologyEx(src,dst,MORPH_TOPHAT,kernel);
	imshow("形态学开闭",dst);
}

void TrackbarBlackhat(int,void*)
{
	int size=sizeTrackbar;
	if(size<1)
		size=1;
	Mat kernel=getStructuringElement(MORPH_RECT,Size(size,size));
	morphologyEx(src,dst,MORPH_BLACKHAT,kernel);
	imshow("形态学开闭",dst);
}

