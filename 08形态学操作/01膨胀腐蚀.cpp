#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

Mat src,dst;
int sizemax=25;
int sizeTrackbar=1;

void Trackbardilate(int,void*);
void Trackbarenrode(int,void*);

int main(int argc, const char *argv[])
{
	src=imread("/home/imx/pic/1.jpg");
	if(!src.data)
	{
		cout<<"load img error"<<endl;
	}
	imshow("input",src);

	Trackbardilate(sizeTrackbar,0);
	createTrackbar("膨胀size","膨胀/腐蚀",&sizeTrackbar,
			sizemax,Trackbardilate);
	Trackbarenrode(sizeTrackbar,0);
	createTrackbar("腐蚀size","膨胀/腐蚀",&sizeTrackbar,
			sizemax,Trackbarenrode);

	waitKey(0);
	return 0;
}

void Trackbardilate(int,void*)
{
	int ksize=sizeTrackbar;
	Mat structElement=getStructuringElement(MORPH_RECT,
			Size(ksize,ksize),Point(-1,-1));
	dilate(src,dst,structElement,Point(-1,-1));
	imshow("膨胀/腐蚀",dst);
}

void Trackbarenrode(int,void*)
{
	int ksize=sizeTrackbar;
	Mat structElement=getStructuringElement(MORPH_RECT,
			Size(ksize,ksize),Point(-1,-1));
	erode(src,dst,structElement);
	imshow("膨胀/腐蚀",dst);
}

