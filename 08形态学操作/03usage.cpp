#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main()
{
	Mat src,gray,dst;
	src=imread("1.png"); //1024*134
	if(!src.data)
	{
		cout<<"load image error"<<endl;
		return -1;
	}
	cout<<"load success"<<endl;
	imshow("src",src);

	cvtColor(src,src,COLOR_BGR2GRAY);
	threshold(src,gray,180,255,THRESH_BINARY_INV);
	imshow("gray",gray);

	Mat imgh=gray.clone();
	Mat Hkernel=getStructuringElement(MORPH_RECT,Size(30,1));
	erode(imgh,imgh,Hkernel);
	dilate(imgh,imgh,Hkernel);
	imshow("A",imgh);

	Mat imgv=gray.clone();
	Mat Vkernel=getStructuringElement(MORPH_RECT,Size(1,30));
	erode(imgv,imgv,Vkernel);
	dilate(imgv,imgv,Vkernel);
	imshow("B",imgv);

	Mat edge=gray.clone();
	Mat Ekernel=getStructuringElement(MORPH_RECT,Size(1,4));
	erode(edge,edge,Ekernel);
	dilate(edge,edge,Ekernel);
	bitwise_not(edge,edge);
	blur(edge,edge,Size(2,2));
	imshow("C",edge);

	waitKey(0);
	return 0;
}
