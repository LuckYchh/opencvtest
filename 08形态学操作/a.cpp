#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
int main(int argc, char** argv) {
	Mat src, dst1,dst2;
	src = imread("/home/chh001/img/1.jpg");
	if (!src.data) {
		printf("could not load image...\n");
		return -1;
	}

	imshow("src", src);

	Mat gray_src;
	cvtColor(src, gray_src, CV_BGR2GRAY);
	
	Mat binImg;
	adaptiveThreshold(~gray_src, binImg, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, -2);

	// 水平结构元素
	Mat hline = getStructuringElement(MORPH_RECT, Size(src.cols / 16, 1), Point(-1, -1));
	// 垂直结构元素
	Mat vline = getStructuringElement(MORPH_RECT, Size(1, src.rows / 16), Point(-1, -1));
	// 矩形结构
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3), Point(-1, -1));

	morphologyEx(binImg, dst1, CV_MOP_OPEN, hline);
	imshow("hline", dst1);
	morphologyEx(binImg, dst2, CV_MOP_OPEN, vline);
	imshow("vline", dst2);

	waitKey(0);
	return 0;
}
