#include "opencvwin.h"

#include <QFileDialog>
#include <QPixmap>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <stdio.h>

using namespace cv;
using namespace std;

//Mat src;

opencvWin::opencvWin(QWidget *parent)
    : QWidget(parent)
{
    label = new QLabel(this);
    label->setMinimumSize(240, 240);

    button = new QPushButton("open",this);

    slidershow();

    QVBoxLayout *vbox1 = new QVBoxLayout;
    vbox1->addWidget(label);
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(button);
    vbox->addWidget(slider);
    vbox->addWidget(slider2);

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addLayout(vbox1);
    hbox->addLayout(vbox);

    setLayout(hbox);

    connect(button,SIGNAL(clicked(bool)),this,SLOT(open_clicked()));
    connect(slider, SIGNAL(valueChanged(int)),this,SLOT(alpha_valuechanged(int)));
    connect(slider2, SIGNAL(valueChanged(int)),this,SLOT(beta_valuechanged(int)));

}

void opencvWin::slidershow()
{
    slider = new QSlider;
    slider2 = new QSlider;
    
    setNum1 = 0;
    slider->setOrientation(Qt::Horizontal);
    slider->setObjectName("alpha");
    slider->setMinimum(1);
    slider->setMaximum(5);
    slider->setValue(setNum1);
    slider->setSingleStep(1);

    setNum2 = 0;
    slider2->setOrientation(Qt::Horizontal);
    slider2->setObjectName("beta");
    slider2->setMinimum(0);
    slider2->setMaximum(50);
    slider2->setValue(setNum2);
    slider2->setSingleStep(5);
}

void opencvWin::brightchange()
{
    Mat m1;
    Mat m2 = src;
    float b,g,r;
    //b=0;g=0;r=0;
    //m1.zeros(m2.size(),m2.type());
    //  cvtColor(m2,m2,CV_BGR2GRAY);
    //m2.convertTo(m1,CV_32F);
    for(int row=0;row<m2.rows;row++)
        for(int col=0;col<m2.cols;col++)
        {
            if(m2.channels()==3)
            {
                b=m1.at<Vec3f>(row,col)[0];
                g=m1.at<Vec3f>(row,col)[1];
                r=m1.at<Vec3f>(row,col)[2];

                m2.at<Vec3b>(row,col)[0]=saturate_cast<uchar>(b*slider->value()+slider2->value());
                m2.at<Vec3b>(row,col)[1]=saturate_cast<uchar>(g*slider->value()+slider2->value());
                m2.at<Vec3b>(row,col)[2]=saturate_cast<uchar>(r*slider->value()+slider2->value());
                printf("alpha:%d,beta:%d\n",slider->value(),slider2->value());
                printf("b:%f g:%f r:%f\n",b,g,r);

            }
            else if(m2.channels()==1)
            {
                float v=m2.at<uchar>(row,col);
                m2.at<uchar>(row,col)=saturate_cast<uchar>(v*slider->value()+slider2->value());
            }
        }

    img = QImage( (unsigned char*)(m2.data), m2.cols, m2.rows, QImage::Format_RGB888 );
    label->update();
    label->setPixmap(QPixmap::fromImage(img));
}

void opencvWin::open_clicked()
{
    img_name = QFileDialog::getOpenFileName( this, tr("Open Image"), ".",
                                                        tr("Image Files(*.png *.jpg *.jpeg *.bmp)"));\

    string pic = img_name.toUtf8().data();
    src = imread(pic);
    if(src.empty())
    {
        printf("can not find the image\n");
        openimg = false;
        return ;
    }
    printf("read image success\n");
    openimg=true;
    cvtColor( src, src, CV_BGR2RGB );
    img = QImage( (const unsigned char*)(src.data), src.cols, src.rows, QImage::Format_RGB888 );
    label->setPixmap(QPixmap::fromImage(img));
}

void opencvWin::alpha_valuechanged(int value)
{
    if(openimg)
    {
       setNum1 = value;
       brightchange();
    }
}

void opencvWin::beta_valuechanged(int value)
{
    if(openimg)
    {
       setNum2 = value;
       brightchange();

    }
}

void opencvWin::close_clicked()
{
    close();
}

opencvWin::~opencvWin()
{

}
