#ifndef OPENCVWIN_H
#define OPENCVWIN_H

#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QLabel>
#include <QImage>

#include <opencv2/opencv.hpp>
using namespace cv;

class opencvWin : public QWidget
{
    Q_OBJECT
public slots:
    void open_clicked();
    void close_clicked();
    void alpha_valuechanged(int value);
    void beta_valuechanged(int value);

public:
    opencvWin(QWidget *parent = 0);
    void slidershow();
    void brightchange();
    ~opencvWin();

    bool openimg;
    int setNum1;
    int setNum2;
    QPushButton *button;
    QLabel *label,*label2;
    QSlider *slider,*slider2;
    QImage img;
    QString img_name;
    Mat src;
};

#endif // OPENCVWIN_H
