#ifndef OPENCVWIN_H
#define OPENCVWIN_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QImage>

class opencvWin : public QWidget
{
    Q_OBJECT
public slots:
    void open_clicked();
    void close_clicked();

public:
    opencvWin(QWidget *parent = 0);
    ~opencvWin();

    QPushButton *button;
    QLabel *label;
    QImage img;
    QString img_name;
};

#endif // OPENCVWIN_H
