#include "opencvwin.h"

#include <QFileDialog>
#include <QPixmap>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <opencv2/opencv.hpp>
#include <stdio.h>
using namespace cv;
using namespace std;


opencvWin::opencvWin(QWidget *parent)
    : QWidget(parent)
{
    label = new QLabel(this);
    label->setMinimumSize(240, 240);

    button = new QPushButton("open",this);

    QVBoxLayout *vbox1 = new QVBoxLayout;
    vbox1->addWidget(label);
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(button);

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addLayout(vbox1);
    hbox->addLayout(vbox);

    setLayout(hbox);

    connect(button,SIGNAL(clicked(bool)),this,SLOT(open_clicked()));

}

void opencvWin::open_clicked()
{
    img_name = QFileDialog::getOpenFileName( this, tr("Open Image"), ".",
                                                        tr("Image Files(*.png *.jpg *.jpeg *.bmp)"));\

    string pic = img_name.toUtf8().data();
    Mat src = imread(pic);
    if(src.empty())
    {
        printf("can not find the image\n");
        return ;
    }
    printf("read image success\n");
    cvtColor( src, src, CV_BGR2RGB );
    img = QImage( (const unsigned char*)(src.data), src.cols, src.rows, QImage::Format_RGB888 );
    label->setPixmap(QPixmap::fromImage(img));
}

void opencvWin::close_clicked()
{
    close();
}

opencvWin::~opencvWin()
{

}
