#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int myLine(Mat *src)
{
	Point p1=Point(20,10);
	Point p2;
	p2.x=200;
	p2.y=210;
	Scalar color=Scalar(0,0,255);
	line(*src,p1,p2,color,1,LINE_AA);
	return 0;
}

int myRectangel(Mat *src,Rect *rect)
{
	*rect=Rect(20,10,20,10);
	Scalar color=Scalar(200,40,78);
	rectangle(*src,*rect,color,2,LINE_8);
	return 0;
}

int myEllipse(Mat *src)
{
	Scalar color=Scalar(10,45,20);
	ellipse(*src,Point((*src).cols/2,(*src).rows/2),Size((*src).cols/4,(*src).rows/8),90,0,360,color,2,LINE_8);
	return 0;
}

int myCircle(Mat *src)
{
	Scalar color=Scalar(34,67,45);
	Point center=Point((*src).cols/2,(*src).rows/2);
	circle(*src,center,150,color,2,8);
	return 0;
}

int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
	Rect rect;
	myLine(&src);
	myRectangel(&src,&rect);
	myEllipse(&src);
	myCircle(&src);
	namedWindow("output",CV_WINDOW_AUTOSIZE);
	imshow("output",src);
	waitKey(0);
	return 0;
}
