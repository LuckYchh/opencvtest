#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat img1=imread("/home/chh001/img/1.jpg");
	Mat img2=imread("/home/chh001/img/2.jpg");
	Mat dst;

	double alpha=0.7;
	if(img1.rows==img2.rows&&img1.cols==img2.cols&&img1.type()==img2.type())
	{
		addWeighted(img1,alpha,img2,(1.0-alpha),0.0,dst);
		multiply(img1,img2,dst,1.0);
		namedWindow("output",CV_WINDOW_AUTOSIZE);
		imshow("output",dst);
	}
	else
	{
		printf("size is not same\n");
	}
	waitKey(0);
	return 0;
}
