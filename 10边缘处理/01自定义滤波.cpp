#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat img=imread("/home/imx/pic/1.jpg");
	if(img.empty())
		cout<<"load img failed"<<endl;
	imshow("input",img);
	
	Mat dst;
	Mat kernel=Mat::zeros(img.size(),img.type());
	char c = 0;
	int index = 0;
	cout<<"======filter2D======="<<endl;
	cout<<"1 自定义滤波(长按有惊喜)"<<endl;
	cout<<"2 SobelX"<<endl;
	cout<<"3 SobelY"<<endl;
	cout<<"4 lapulasi"<<endl;
	while(true) 
	{
		c = waitKey(500);
		if(c == 27) 
			break;
		if(c==49)
		{	
			int ksize = 5 + (index % 8) * 2;
			kernel = Mat::ones(Size(ksize, ksize), CV_32F) / (float)(ksize * ksize);
			index++;
		}
		else if(c==50)
  			kernel=(Mat_<char>(3,3)<<-1,0,1,-2,0,2,-1,0,1);
		else if(c==51)
			kernel=(Mat_<char>(3,3)<<-1,-2,-1,0,0,0,1,2,1);
		else if(c==52)
			kernel=(Mat_<char>(3,3)<<0,-1,0,-1,4,-1,0,-1,0);
		filter2D(img,dst,-1,kernel);
		imshow("filter2D实验",dst);
	}

	waitKey(0);
	return 0;
}
