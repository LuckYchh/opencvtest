#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
	}
	imshow("input",src);

	Mat dst;
	int top=20;
	int bottom=20;
	int left=20;
	int right=20;
	int c=0;
	RNG rng(500);
	int borderType=BORDER_DEFAULT;
	printf("======MEUE========\n");
	printf("1 BORDER_REPLICATE\n");
	printf("2 BORDER_REFLECT\n");
	printf("3 BORDER_REFLECT_101\n");
	printf("4 BORDER_WRAP\n");
	printf("5 BORDER_CONSTANT\n");
	printf("6 BORDER_TRANSPARENT\n");
	while(true)
	{
		c=waitKey(500);
		if(c==27)
			break;
		if(c==49)
			borderType=BORDER_REPLICATE;
		else if(c==50)
			borderType=BORDER_REFLECT;
		else if(c==51)
			borderType=BORDER_REFLECT_101;
		else if(c==52)
			borderType=BORDER_WRAP;
		else if(c==53)
			borderType=BORDER_CONSTANT;
		else if(c==54)
			borderType=BORDER_TRANSPARENT;
		Scalar value=Scalar(rng.uniform(0,255),
				rng.uniform(0,255),rng.uniform(0,255));
		copyMakeBorder(src,dst,top,bottom,left,right,borderType,value);
		imshow("copyMakeBorder边缘效果",dst);

	}
	waitKey(0);
	return 0;
}
