#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main()
{
	Mat src,dst;
	src = imread("/home/chh001/img/1.jpg");
	if (!src.data) {
		printf("could not load image...\n");
		return -1;
	}
	imshow("input", src);
	cvtColor(src,src,COLOR_BGR2GRAY);
	blur(src, src, Size(3, 3));
	int t1_value = 50;
	char c = 0;
	printf("----------------\n");
	printf("数字键1为+\n");
	printf("数字键2为-\n");

	while(true)
	{
		c = waitKey(500);
		if(c == 49)
		{
			t1_value += 20;
			printf("t1_value:%d \n",t1_value);
		}
		else if(c == 50)
		{
			t1_value -= 20;
			printf("t1_value:%d \n",t1_value);
		}

		Canny(src, dst, t1_value, t1_value*2);
		imshow("canny检测",dst);
	}

	waitKey(0);
	return 0;
}

