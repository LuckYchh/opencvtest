#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat src=imread("/home/chh001/img/1.jpg");
	if(src.empty())
	{
		cout<<"load img error"<<endl;
	}
	cvtColor(src,src,CV_BGR2GRAY);
	imshow("input",src);
	Mat dst=Mat(src.size(),src.type());
	printf("====边缘处理======\n");
	printf("1 Sobelx\n");
	printf("2 Sobely\n");
	printf("3 Scharrx\n");
	printf("4 Scharry\n");
	printf("5 lapulasi\n");
	char c;
	while(true)
	{
		c=waitKey(500);
		if(c==49)
			Sobel(src,dst,CV_16S,1,0,3);
		else if(c==50)
			Sobel(src,dst,CV_16S,0,1,3);
		else if(c==51)
			Scharr(src,dst,CV_16S,1,0);
		else if(c==52)
			Scharr(src,dst,CV_16S,0,1);
		else if(c==53)
			Laplacian(src,dst,CV_16S,3);
		convertScaleAbs(dst,dst);
		imshow("边缘处理",dst);

	
	}
	waitKey(0);
	return 0;
}
