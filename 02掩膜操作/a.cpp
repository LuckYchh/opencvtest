#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat img=imread("/home/imx/pic/1.jpg");
	Mat dst;
	imshow("input",img);
	
	Mat kernel=(Mat_<char>(3,3)<<0,-1,0,-1,3,-1,0,-1,0);
	filter2D(img,dst,img.depth(),kernel);
	imshow("output",dst);
	waitKey(0);
	return 0;
}
