#include <opencv2/opencv.hpp>
#include <iostream>
#include <math.h>

using namespace cv;

int main(int argc, char** argv) 
{
	Mat src = imread("/home/chh001/img/1.jpg");
	if (!src.data) {
		printf("could not load image...\n");
		return -1;
	}
	imshow("input", src);

	Mat map_x,map_y,dst;
	map_x.create(src.size(), CV_32FC1);
	map_y.create(src.size(), CV_32FC1);

	char c = 0;
	printf("=====remap======\n");
	printf("0 尺寸变换\n");
	printf("1 X变换\n");
	printf("2 Y变换\n");
	printf("3 原点变换\n");
	while (true)
	{
		c = waitKey(500);
		if (c == 27)
			break;
		int index = c % 4;
		for (int row = 0; row < src.rows; row++) {
			for (int col = 0; col < src.cols; col++) {
				switch (index) {
				case 0:
					if (col > (src.cols * 0.25) && col <= (src.cols*0.75) && row > (src.rows*0.25) && row <= (src.rows*0.75)) {
						map_x.at<float>(row, col) = 2 * (col - (src.cols*0.25));
						map_y.at<float>(row, col) = 2 * (row - (src.rows*0.25));
					}
					else {
						map_x.at<float>(row, col) = 0;
						map_y.at<float>(row, col) = 0;
					}
					break;
				case 1:
					//1 2 3 4 5 6
					map_x.at<float>(row, col) = (src.cols - col );
					map_y.at<float>(row, col) = row;
					break;
				case 2:
					map_x.at<float>(row, col) = col;
					map_y.at<float>(row, col) = (src.rows - row );
					break;
				case 3:
					map_x.at<float>(row, col) = (src.cols - col );
					map_y.at<float>(row, col) = (src.rows - row );
					break;
				}

			}
		}
		remap(src, dst, map_x, map_y, INTER_NEAREST, BORDER_CONSTANT, Scalar(23, 45, 100));
		imshow("remap", dst);
	}
	waitKey(0);
	return 0;
}

