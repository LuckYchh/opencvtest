#include <iostream>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


int main(int argc, const char *argv[])
{
	Mat img=imread("xxxx");
	imshow("input",img);

	Mat dst1=Mat(img.size(),img.type());
	dst1=Scalar(127,0,255);
	imshow("dst1",dst1);

	Mat dst2;
	img.copyTo(dst2);
	cvtColor(img,dst2,CV_BGR2GRAY);
	imshow("dst2",dst2);

	Mat dst3;
	dst3.create(img.size(),img.type());
	dst3=Scalar(0,0,255);
	imshow("dst3",dst3);

	waitKey(0);
	return 0;
}
